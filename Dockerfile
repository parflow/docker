#RMM ParFlow test

# start by building the basic container
FROM centos:latest
MAINTAINER Reed Maxwell <rmaxwell@mines.edu>

#

RUN yum  install -y  gcc  \
    build-essential \
    cmake3 \
    make \
    g++  \
    gcc-c++  \
    gdc \
    gcc-gfortran \
    tcl-devel \
    tk-dev \
    git
RUN yum install -y openmpi openmpi-devel
RUN yum install -y zlib

#RUN whereis mpicc
#RUN alias which=/usr/bin/whereis
#RUN mpirun --version


# make directories
RUN mkdir /home/parflow
#RUN mkdir parflow
#RUN mkdir silo
#RUN mkdir hypre
# set environment vars

#ENV CC gcc
#ENV CXX g++
#ENV FC gfortran
#ENV F77 gfortran
#ENV PARFLOW_DIR /home/parflow/parflow_build
ENV PARFLOW_DIR /home/parflow/parflow
ENV SILO_DIR /home/parflow/silo-4.10.2
ENV HYPRE_DIR /home/parflow/hypre
ENV PATH /usr/local/bin/:/usr/bin/gcc:/usr/lib/gcc:/usr/libexec/gcc:$PATH

# These take the place of "module load mpi" which doesn't seem to work in Dockerfiles
ENV MPI_INCLUDE /usr/include/openmpi-x86_64
ENV MPI_BIN /usr/lib64/openmpi/bin
ENV MPI_LIB /usr/lib64/openmpi/lib
ENV MPI_SYSCONFIG /etc/openmpi-x86_64
ENV MPI_COMPILER openmpi-x86_64
ENV MPI_HOME /usr/lib64/openmpi
ENV MPI_SUFFIX _openmpi
ENV MPI_MAN /usr/share/man/openmpi-x86_64
ENV MPI_FORTRAN_MOD_DIR /usr/lib64/gfortran/modules/openmpi-x86_64
ENV MPI_PYTHON_SITEARCH /usr/lib64/python2.7/site-packages/openmpi
ENV MANPATH /usr/share/man/openmpi-x86_64:/usr/share/man:/usr/local/share/man
ENV PYTHONPATH /usr/lib64/python2.7/site-packages/openmpi
ENV PATH /usr/lib64/openmpi/bin:${PATH}:/usr/lib64/openmpi-x86_64/bin/:/usr/include/openmpi-x86_64/
ENV LD_LIBRARY_PATH ${MPI_LIB}:${LD_LIBRARY_PATH}

#RUN pwd
#RUN ls

# build libraries
#SILO

WORKDIR /home/parflow
RUN curl "https://wci.llnl.gov/content/assets/docs/simulation/computer-codes/silo/silo-4.10.2/silo-4.10.2.tar.gz" -o "silo-4.10.2.tar.gz"
RUN tar -xvf silo-4.10.2.tar.gz

WORKDIR $SILO_DIR

RUN ./configure  --prefix=$SILO_DIR --disable-silex --disable-hzip --disable-fpzip
RUN make install

#Hypre
WORKDIR /home/parflow

RUN git clone -b master --single-branch https://github.com/LLNL/hypre.git hypre

WORKDIR $HYPRE_DIR/src
RUN ./configure --prefix=$HYPRE_DIR --with-MPI
RUN make install

WORKDIR /home/parflow
RUN git clone -b docker --single-branch https://gitlab.kitware.com/parflow/parflow.git parflow

# build ParFlow
WORKDIR $PARFLOW_DIR/pfsimulator
RUN ./configure --prefix=$PARFLOW_DIR --with-clm --enable-timing --with-silo=$SILO_DIR --with-hypre=$HYPRE_DIR --with-amps=mpi1 --with-mpi
RUN make install


# build PFTools
WORKDIR $PARFLOW_DIR/pftools
RUN ./configure --prefix=$PARFLOW_DIR   --with-silo=$SILO_DIR  --with-amps=mpi1
RUN make install

# test
# WORKDIR $PARFLOW_DIR/test
# RUN make check
ENV PATH /usr/lib64/openmpi/bin:${PATH}:/usr/lib64/openmpi-x86_64/bin/:/usr/include/openmpi-x86_64:${PARFLOW_DIR}/bin


#RUN  cmake ../parflow \
#        -DPARFLOW_AMPS_LAYER=seq \
#	-DHYPRE_ROOT=$HYPRE_DIR \
#	-DSILO_ROOT=$SILO_DIR \
#	-DPARFLOW_ENABLE_TIMING=TRUE \
#	-DPARFLOW_HAVE_CLM=ON \
#	-DCMAKE_INSTALL_PREFIX=$PARFLOW_DIR


##
WORKDIR $PARFLOW_DIR/test
# The default command:
# 1. Uses bash. This ensures environment variables like LD_LIBRARY_PATH are set.
# 2. Runs tclsh to invoke one of the ParFlow test runs.
#    This runs the parflow executable indirectly via the
#    /home/parflow/parflow/bin/run script (created during
#    the build from pfsimulator/amps/mpi1/run).
CMD ["bash", "-c", "tclsh default_single.tcl 1 1 1"]
